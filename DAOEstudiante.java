/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import POJO.Estudiante;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author samsung
 */
public class DAOEstudiante {

    public List<Estudiante> Estudiantes = new ArrayList();

    public void AnhadirEstudiante(Estudiante e) {
        if (e != null) {
            Estudiantes.add(e);
        } else {
            System.out.println("Ingrese estudiante");
        }
    }

    public boolean EditarEstudiante(Estudiante e) {
        boolean flagEditado = false;
        String carnet = e.getNoCarnet();
        Estudiante est = BuscarPorCarnet(carnet);
        if (est != null) {
            int indice = Estudiantes.indexOf(est);
            Estudiantes.set(indice, e);
            flagEditado = true;
        } else {
            System.out.println("Estudiante No Modificado");
        }

        return flagEditado;
    }

    public Estudiante BuscarPorCarnet(String carnet) {
        boolean FlagEncontrado = false;
        Estudiante est = null;
        for (Estudiante e : Estudiantes) {
            if (e.getNoCarnet().equalsIgnoreCase(carnet)) {
                System.out.println("Estudiante Encontrado");
                System.out.println("Estudiante: " + e.getNombres() + " " + e.getApellidos());
                System.out.println("Carnet:" + e.getNoCarnet());
                System.out.println("Edad: " + e.getEdad());
                int sumaNotas = 0;
                float promedio = 0.2f;
                for (Object nota : e.getNotas()) {
                    sumaNotas += Float.parseFloat(nota.toString());
                }
                promedio = (float) (sumaNotas / e.getNotas().size());
                System.out.println("Promedio: " + promedio);
                FlagEncontrado = true;
                est = e;
            }
            if (FlagEncontrado) {
                break;
            }
        }
        if (!FlagEncontrado) {
            System.out.println("Estudiante No encontrado");
        }
        return est;
    }

    public boolean EliminarEstudiante(String carnet) {
        boolean flagEliminado = false;
        Estudiante e = BuscarPorCarnet(carnet);

        int index = Estudiantes.indexOf(e);
        if (index >= 0) {
            Estudiantes.remove(index);
            flagEliminado = true;
        } else {
            System.out.println("El estudiante no fue encontrado");
        }

        return flagEliminado;
    }

    public void MostrarTodos() {
        for (Estudiante e : Estudiantes) {
            int sumaNotas = 0;
            float promedio = 0.2f;
            for (Object nota : e.getNotas()) {
                sumaNotas += Float.parseFloat(nota.toString());
            }
            promedio = (float) (sumaNotas / e.getNotas().size());
            System.out.println("_____________________________________");
            System.out.println("Nombres: " + e.getNombres());
            System.out.println("Apellidos: " + e.getApellidos());
            System.out.println("No. Carnet: " + e.getNoCarnet());
            System.out.println("Promedio: " + promedio);
            System.out.println("______________________________________");
            System.out.println("");
        }
    }
}
