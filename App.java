/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import DAO.DAOEstudiante;
import POJO.Estudiante;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author samsung
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        DAOEstudiante DAO = new DAOEstudiante();
        int opcMenu = 0;
        int opcSeguir = 1;
        do {
            do {
                System.out.println("Sistema de Registro Académico");
                System.out.println("Elija su opción");
                System.out.println("1. Añadir Estudiante");
                System.out.println("2. Editar Estudiante");
                System.out.println("3. Eliminar Estudiante");
                System.out.println("4. Buscar Estudiante por Carnet");
                System.out.println("5. Mostrar Todos Los Estudiantes");

                opcMenu = sc.nextInt();

                if (opcMenu < 1 || opcMenu > 5) {
                    System.out.println("Digite una opción correcta");
                }
            } while (opcMenu < 1 || opcMenu > 5);

            switch (opcMenu) {
                case 1: {
                    System.out.println("Nombres:");
                    sc.nextLine();
                    String Nombre = sc.nextLine();
                    System.out.println("Apellidos: ");
                    String Apellidos = sc.nextLine();
                    System.out.println("Carnet:");
                    String carnet = sc.nextLine();
                    System.out.println("Edad");
                    int edad = sc.nextInt();
                    Estudiante e = new Estudiante();
                    int opcionSeguirNotas = 0;
                    do {
                        System.out.println("Nueva Nota:");
                        float Nota = sc.nextFloat();
                        e.getNotas().add((Object) Nota);
                        System.out.println("Nota Registrada. ¿Desea ingresar una nueva nota?");
                        System.out.println("0. NO 1. SI");
                        opcionSeguirNotas = sc.nextInt();
                    } while (opcionSeguirNotas == 1);

                    e.setNombres(Nombre);
                    e.setApellidos(Apellidos);
                    e.setNoCarnet(carnet);
                    e.setEdad(edad);
                    DAO.AnhadirEstudiante(e);
                    break;
                }

                case 2: {
                    System.out.println("Digite el carnet del estudiante");
                    sc.nextLine();
                    String carnet = sc.nextLine();
                    Estudiante e = DAO.BuscarPorCarnet(carnet);

                    if (e != null) {
                        System.out.println("Nombres:");
                        String Nombre = sc.nextLine();
                        System.out.println("Apellidos: ");
                        String Apellidos = sc.nextLine();
                        System.out.println("Edad:");
                        int edad = sc.nextInt();
                        e.setNombres(Nombre);
                        e.setApellidos(Apellidos);
                        e.setEdad(edad);

                        DAO.EditarEstudiante(e);
                    }
                    break;
                }

                case 3: {
                    System.out.println("Digite el carnet del estudiante");
                    sc.nextLine();
                    String carnet = sc.nextLine();
                    DAO.EliminarEstudiante(carnet);
                    break;
                }
                case 4: {
                    System.out.println("Digite el carnet del estudiante");
                    sc.nextLine();
                    String carnet = sc.nextLine();
                    System.out.println("////////////////////////////////////////");
                    Estudiante e = DAO.BuscarPorCarnet(carnet);
                    System.out.println("////////////////////////////////////////");

                    break;
                }

                case 5: {
                    DAO.MostrarTodos();
                    break;
                }
            }
            System.out.println("Desea seguir en el sistema");
            System.out.println("1. SI");
            System.out.println("0. NO");
            opcSeguir = sc.nextInt();
        } while (opcSeguir != 0);

    }

}
