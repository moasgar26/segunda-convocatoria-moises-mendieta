/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package POJO;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author samsung
 */
public class Estudiante {
    String nombres;
    String apellidos;
    String noCarnet;
    List<Float> Notas;
    int edad;

    public Estudiante() {
        this.Notas = new ArrayList();
    }

    public Estudiante(String nombres, String apellidos, String noCarnet, int edad) {
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.noCarnet = noCarnet;
        this.Notas = new ArrayList();
        this.edad = edad;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getNoCarnet() {
        return noCarnet;
    }

    public void setNoCarnet(String noCarnet) {
        this.noCarnet = noCarnet;
    }

    public List getNotas() {
        return Notas;
    }

    public void setNotas(List Notas) {
        this.Notas = Notas;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }
    
    
}
